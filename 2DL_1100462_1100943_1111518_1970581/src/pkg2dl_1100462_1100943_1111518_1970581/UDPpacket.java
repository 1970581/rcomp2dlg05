/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que simula o um protocolo UDP e o seu PAYLOAD para enviar listas de ficheiros. Serve para facilitar o codigo.
 * @author Hugo
 */
public class UDPpacket {
    
    /** Cadeia de bytes para inserir no pacote UDP, guardada nesta classe */
    private byte[] payload;
    
    public static final String  PROTOCOL_NAME = "UDP_0001";
    public static final int     PROTOCOL_PAYLOAD_SIZE = 512;
    
    
    public static final int BYTE_PROTOCOL_START = 0;
    public static final int BYTE_PROTOCOL_END = 7;
    public static final int BYTE_PROTOCOL_LENGTH = 8;
    
    public static final int BYTE_VERSAO_LOCATION = 9;
    
    public static final int BYTE_PC_START = 10;
    public static final int BYTE_PC_END = 17;
    public static final int BYTE_PC_LENGTH = 8;
    
    public static final int BYTE_PORTA_START = 19;
    public static final int BYTE_PORTA_END = 22;
    public static final int BYTE_PORTA_LENGTH = 4;
    
    public static final int BYTE_TEMPO_START = 23;
    public static final int BYTE_TEMPO_END = 30;
    public static final int BYTE_TEMPO_LENGTH = 8;
    
    public static final int     BYTE_LIST_START = 31;
    public static final int     BYTE_LIST_LENGTH = PROTOCOL_PAYLOAD_SIZE - BYTE_LIST_START;
    
    public static final String  REGEX_FILENAME_SEPARATOR = "/";
    // http://stackoverflow.com/questions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
    
    
    //0-7   nome do protocolo.
    //8     zero.
    //9     versao
    //10-17 nome do PC
    //18    zero
    //19-22 porta int 4 bytes
    //23-30 tempo long 8 bytes
    //31-512 texto
    
    /**
     * Construtor com parametros.
     */
    public UDPpacket( String nomeProtocolo, byte versao, String nomePC, int porta, long tempo  ){
        
        this.payload = new byte[PROTOCOL_PAYLOAD_SIZE];
        
        for(int i = 0; i< PROTOCOL_PAYLOAD_SIZE; i++){ //zerar o array
            this.payload[i] = 0;
        }
        
        //Inserir nome de protocolo.
        byte[] bytesNomeProtocolo = nomeProtocolo.getBytes();
        if(bytesNomeProtocolo.length <= BYTE_PROTOCOL_LENGTH){// Introduzir o nome do protocolo.
            System.arraycopy(bytesNomeProtocolo, 0, this.payload, 0, bytesNomeProtocolo.length);
        }
        
        //inserir versao do protocolo
        this.payload[BYTE_VERSAO_LOCATION] = versao;
        
        // Inserir nome do PC.
        byte[] bytesNomePC = nomePC.getBytes();
        if(bytesNomePC.length <= BYTE_PC_LENGTH){
            System.arraycopy( bytesNomePC , 0, this.payload, BYTE_PC_START, bytesNomePC.length);
        }
        
        //inserir nº porta
        ByteBuffer bufferPorta = ByteBuffer.allocate(BYTE_PORTA_LENGTH);
        bufferPorta.putInt(porta);
        byte[] bytesPorta = bufferPorta.array();
        System.arraycopy( bytesPorta, 0 , this.payload, BYTE_PORTA_START, bytesPorta.length);
        
        //inserir tempo long
        ByteBuffer bufferTempo = ByteBuffer.allocate(BYTE_TEMPO_LENGTH);
        bufferTempo.putLong(tempo);
        byte[] bytesTempo = bufferTempo.array();
        System.arraycopy( bytesTempo , 0 , this.payload, BYTE_TEMPO_START, bytesTempo.length);
    }
    
    /** Constroi um objecto indicando o payload, para ser mais facil extrair dados. */
    public UDPpacket(byte[] payload512BytesOrBust){
        //Se nao tiver 512 bytes falha logo no construtor.
        if (payload512BytesOrBust.length != UDPpacket.PROTOCOL_PAYLOAD_SIZE) throw new IllegalArgumentException();
        this.payload = new byte[PROTOCOL_PAYLOAD_SIZE];
        System.arraycopy( payload512BytesOrBust , 0 , this.payload, 0, payload512BytesOrBust.length);
    }
    
    /** obtem o payload dentro do objecto */
    public byte[] obterPayload(){return this.payload;}
    
    /** Inserir um nome de ficheiro no payload*/
    public boolean inserirFicheriroNoPayload(String nomeFicheiro){
        //Acrescentamos um \n ao string que recebemos.
        String insersao = nomeFicheiro.concat(REGEX_FILENAME_SEPARATOR);
        
        //Depois convertemos os bytes da lista actual num string, concatenamos o novo ficheiro.
        String actual = new String(this.payload , BYTE_LIST_START, BYTE_LIST_LENGTH);
        actual = actual.trim();
        String novo = actual.concat(insersao);
        
        //Reconvertemos o string concatenado para bytes, e se for maior que o tamanha retornamos falso.
        byte[] byteNovaLista = novo.getBytes();
        if(byteNovaLista.length > BYTE_LIST_LENGTH) return false;
        
        //Senao, copiamos para o pacote, e retornamos true.
        System.arraycopy(byteNovaLista, 0, this.payload, BYTE_LIST_START, byteNovaLista.length);
        return true;
    }
    
    /** Converte os bytes corespondente a lista de nomes de ficheiros numa lista de strings de nomes de ficheiro */
    public List<String> obterListaFicheiro(){
        List <String> lista = new ArrayList();
        String actual = new String(this.payload , BYTE_LIST_START, BYTE_LIST_LENGTH);
        actual = actual.trim();
        String vector[] = actual.split(REGEX_FILENAME_SEPARATOR);
        for(String s :vector){
            lista.add(s);
        }
        
        return lista;
    }
    
    
   
    /** obtem o nome do protocolo inscrito no payload */
    public String getNomeProtocolo(){
        byte byteNomeProtocolo[] = new byte[BYTE_PROTOCOL_LENGTH];
        System.arraycopy( this.payload , BYTE_PROTOCOL_START , byteNomeProtocolo, 0, BYTE_PROTOCOL_LENGTH);
        String nomeProtocolo = new String(byteNomeProtocolo, 0, byteNomeProtocolo.length);
        return nomeProtocolo.trim();
    }
    
    public byte getVersao(){
        return this.payload[BYTE_VERSAO_LOCATION];
    }
    
    public String getNomePc(){
        byte byteNomePc[] = new byte[BYTE_PC_LENGTH];
        System.arraycopy( this.payload, BYTE_PC_START, byteNomePc, 0, BYTE_PC_LENGTH);
        String nomePc = new String(byteNomePc, 0, byteNomePc.length);
        return nomePc.trim();
    }
    
    public int getPorta(){
        byte bytePorta[] = new byte[BYTE_PORTA_LENGTH];
        System.arraycopy(this.payload, BYTE_PORTA_START, bytePorta, 0, BYTE_PORTA_LENGTH);
        ByteBuffer bufferPorta = ByteBuffer.wrap(bytePorta);
        return bufferPorta.getInt();
    }
    
    public long getTempo(){
        byte byteTempo[] = new byte[BYTE_TEMPO_LENGTH];
        System.arraycopy(this.payload, BYTE_TEMPO_START, byteTempo, 0, BYTE_TEMPO_LENGTH);
        ByteBuffer bufferTempo = ByteBuffer.wrap(byteTempo);
        return bufferTempo.getLong();
    }
}

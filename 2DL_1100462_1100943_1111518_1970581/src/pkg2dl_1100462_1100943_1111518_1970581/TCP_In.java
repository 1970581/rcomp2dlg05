/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que lida com a recepcao de dados da ligacao TCP.
 * @author Hugo
 */
public class TCP_In implements Runnable{
    
    
    private Socket conectionSocket;
    private Shared shared;
    String diretorioLeitura;
    RepositorioFicheiroDistante repositorio;
    
    
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private TCPpacket tcp;
    private byte[] fileNameBytes;
    
    public TCP_In(Socket conectionSocket, Shared shared, String diretorioLeitura ){
        this.conectionSocket = conectionSocket;
        this.shared = shared;
        this.diretorioLeitura = diretorioLeitura;
        this.repositorio = repositorio;
    }

    @Override
    public void run() {
        boolean messageOk = false;
        System.out.println("TCP_In: nova ligacao de " + conectionSocket.getInetAddress().getHostAddress() + ":"+ conectionSocket.getPort());
        
        try {
            sOut    = new DataOutputStream( conectionSocket.getOutputStream() );
            sIn     = new DataInputStream(  conectionSocket.getInputStream()  );
        
        
        //Obter o 1 header.
            messageOk = readTCP();
        
        //Se nao for uma mensagem de pedido de download saimos.
            if( !messageOk || !this.tcp.isMessageType(TCPpacket.MessageType.MESSAGE_ASK_TO_DOWNLOAD)){
                System.out.println("TCP_In: Incorrect header read. Droping this upload link.");
                conectionSocket.close();
                return;
            }
        
            //Vamos read o nome do ficheiro
            messageOk = readFilename(this.tcp.readDataSize());
            if (!messageOk) {
                System.out.println("TCP_In: Incorrect filename get from in buffer. Droping this upload link.");
                terminarClose();
                return;
            }
            String filename = new String(this.fileNameBytes);
        
            //Verificamos se o ficheiro em questao existe.
            boolean fileExists = ListarDiretorio.ficheiroExiste(diretorioLeitura,filename);
            
            //Se o ficheiro nao existir, emviamos mensagem de sair e dropamos o link.
            if(!fileExists) {
                enviarMensagemSair();
                System.out.println("TCP_In: Pedido de ficheiro inexistente. Enviei mensagem de sair.");
                terminarClose();
                return;
            }
            
            //Exitste, vamos escrever.
            boolean ficheiroEnviado = this.enviarFicheiro(filename);
            if (!ficheiroEnviado){
                this.enviarMensagemSair();
                System.out.println("TCP_In: File upload failure. -> " + filename);
                this.terminarClose();
                return;
            }
            
            System.out.println("TCP_In: File upload sucess. -> " + filename);
            
            //zzz(5);//sleep 5 sec
            System.out.println("TCP_In: terminating thread.");
        
        }
        catch(IOException ioEx){
            System.out.println("TCP_In: Exception:  " + ioEx);
        }
        
        terminarClose();    //FIM.
    }
    
    
    private void zzz(int time){
        long timelong = time *1000;
        try {
            sleep(timelong);
        } catch (InterruptedException ex) {
            System.out.println("TCP_IN WOKE FROM SLEEP!!!");
            Logger.getLogger(TCP_In.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private boolean enviarFicheiro(String filename) throws IOException{
        String completeFilePath = this.diretorioLeitura + filename;
        
        File file = new File(completeFilePath);
        long fileSizeLong = file.length();
        if (fileSizeLong > 100000000) return false; // Se tamanho mais que 100  megas, ignora. max 2,147,483,647
        int fileSize = (int) fileSizeLong;
        System.out.println("TCP_In: File-upload size: "+fileSizeLong+":"+fileSize);
        
        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TCP_In.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("TCP_In: Cant find file::  " + ex);
            return false;
        }
        
        BufferedInputStream bis = new BufferedInputStream(fis); 
        
        byte byteFile[] = new byte[fileSize];
        
        bis.read(byteFile);
        
        
        TCPpacket tcpFileHeader = new TCPpacket(TCPpacket.MessageType.MESSAGE_FILE, fileSize);
        
        sOut.write(tcpFileHeader.readHeaderPayload());
        
        sOut.write(byteFile);
        
        bis.close();
        fis.close();
        
        return true;
    }
    
    
    
    private void enviarMensagemSair() throws IOException{
        TCPpacket tcpSair = new TCPpacket(TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT, 0);
        sOut.write(tcpSair.readHeaderPayload());
    }
    
    
    /**
     * Faz read do nome do ficheiro.
     * @param filenameLenght numero de bytes a ler para do stream do nome do ficheiro
     * @return false se apanhou um erro, true se ok.
     * @throws IOException 
     */
    private boolean readFilename(int filenameLenght) throws IOException{
        this.fileNameBytes = new byte[filenameLenght];
        int nBytesLidos = 0;
        nBytesLidos = sIn.read(this.fileNameBytes);
        
        return (nBytesLidos == filenameLenght);
    }
    
    
    
    /**
     * Funcao que le 32 bytes e os coloca na variavel this.tcp 
     * @return falso se apanhou um erro, true se tudo ok.
     * @throws IOException 
     */
    private boolean readTCP() throws IOException{
        
        int headerSize = TCPpacket.HEADER_SIZE;
        int nBytesLidos = 0;
        
        byte packetHeader[] = new byte[headerSize];
        nBytesLidos = sIn.read(packetHeader);
        if(nBytesLidos != headerSize) {
            return false;
        }
        tcp = new TCPpacket(packetHeader);
        return tcp.isMyProtocol();
    }
    
    
    /** indica para terminar*/
    private void terminarClose(){
        try {
            sIn.close();
            sOut.close();
            this.conectionSocket.close();
        } catch (IOException ex) {
            System.out.println("TCP_In: .terminar() Exception:  " + ex);
        }
    }
    
    
   
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugo
 */
public class ReceptorUDP implements Runnable{
    
    private Shared shared;       
    private int portEscutaTCP;
    private RepositorioFicheiroDistante repositorio;
    
    private InetAddress meuIp;
    
    private static final int MAX_WAIT_TIME_IN_SOCKET = 5000;
    
    
    
    
    public ReceptorUDP( RepositorioFicheiroDistante repositorio, Shared shared){
        this.shared = shared;
        this.repositorio = repositorio;
        portEscutaTCP = EmisorUDP.PORT_RECEPCAO_BROADCAST;
    }

    @Override
    public void run() {
        
        try {            this.meuIp = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(ReceptorUDP.class.getName()).log(Level.SEVERE, null, ex);
            shared.terminarAplicacao(true);
            System.out.println("ReceptorUDP constructor UnknownHostException: " + ex.getMessage());
            return;
        }
        
        //Criar socket
        DatagramSocket sock;
        try {        
            sock = new DatagramSocket(this.portEscutaTCP);
            sock.setSoTimeout(MAX_WAIT_TIME_IN_SOCKET);
        } catch (SocketException ex) {
            Logger.getLogger(ReceptorUDP.class.getName()).log(Level.SEVERE, null, ex);
            shared.terminarAplicacao(true);
            System.out.println("ReceptorUDP sock creation error: " + ex.getMessage());
            return;
        }
        
        byte[] data = new byte[UDPpacket.PROTOCOL_PAYLOAD_SIZE];
        DatagramPacket udpPacket = new DatagramPacket(data, data.length);
        
                //SocketTimeoutException
        while(true){
            if(shared.terminarAplicacao(false) == true) break;
            data = new byte[UDPpacket.PROTOCOL_PAYLOAD_SIZE];
            udpPacket.setData(data);
            udpPacket.setLength(data.length);
            
            boolean timeout = false;
            
            try {
                sock.receive(udpPacket);
            } 
            catch (SocketTimeoutException ex){
                timeout = true;
                //System.out.println("ReceptorUDP socket timeout.");            ************************************** DEBUG
                continue; // recomeca o ciclo se 
            }
            catch (IOException ex) {
                Logger.getLogger(ReceptorUDP.class.getName()).log(Level.SEVERE, null, ex);
                shared.terminarAplicacao(true);
                System.out.println("ReceptorUDP sock.receive() IOException error: " + ex.getMessage());
                return;
            }
            
            //REcebemos um pacote.
            InetAddress endressoPacote = udpPacket.getAddress();
            int comprimentoPacote = udpPacket.getLength();
            System.out.println("ReceptorUDP: recebido pacote de " + comprimentoPacote +" bytes de "+ endressoPacote.getHostAddress() + ".");
            if (comprimentoPacote == UDPpacket.PROTOCOL_PAYLOAD_SIZE) escreverDados(data, endressoPacote);
            
        }
        
        
        System.out.println("ReceptorUDP terminating...");
    }
    
    
    /**
     * Metodo para escrever os dados recebidos.
     * @param dados
     * @param endressoOwner
     * @return 
     */
    private boolean escreverDados(byte[] dados, InetAddress endressoOwner){
        UDPpacket pacote = new UDPpacket(dados);
        long tempoRecebido = System.currentTimeMillis();
        
        String protocolo = pacote.getNomeProtocolo();
        byte versao = pacote.getVersao();
        
        if(!protocolo.equals(UDPpacket.PROTOCOL_NAME) ) {
            System.out.println("ReceptorUDP recebi um pacote com protocolo incorrecto. Descartei.");
            return false;
        }
        
        String nomePc = pacote.getNomePc();
        int porta = pacote.getPorta();
        List<String> listaNomeFicheiros = pacote.obterListaFicheiro();
        
        for(String nomeFicheiro : listaNomeFicheiros){
            FicheiroDistante entrada = new FicheiroDistante(nomePc, nomeFicheiro, tempoRecebido, endressoOwner, porta);
            try {
                this.repositorio.adicionarFicheiroDistante(entrada);
            } catch (InterruptedException ex) {
                Logger.getLogger(ReceptorUDP.class.getName()).log(Level.SEVERE, null, ex);
                shared.terminarAplicacao(true);
                System.out.println("ReceptorUDP adicionarFicheiroDistante InterruptedException error: " + ex.getMessage());
            }
        }
        
        
        return true;
    }
    
    
    
    
   
    
}

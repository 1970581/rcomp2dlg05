/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 *
 * @author Hugo
 */
public class FicheiroDistante implements Comparable<FicheiroDistante>{
    private String pcName;
    private String nomeFicheiro;
    private long  tempoRecebido;
    private int portaTCPparaDownload;
    private InetAddress ipOwnerParaDownload;
    
    
    public FicheiroDistante(String pcName, String nomeFicheiro, long tempoRecebido, InetAddress ipOwnerParaDownload, int portaTCPparaDownload ){
        this.pcName = pcName;
        this.nomeFicheiro = nomeFicheiro;
        this.tempoRecebido = tempoRecebido;
        this.portaTCPparaDownload = portaTCPparaDownload;
        this.ipOwnerParaDownload = ipOwnerParaDownload;
    }
    
    public FicheiroDistanteDTO criarDTO(){
        return new FicheiroDistanteDTO(this.pcName, this.nomeFicheiro);
    }

    public String getPcName() {        return pcName;    }
    public String getNomeFicheiro() {  return nomeFicheiro;    }
    public long getTempoRecebido() {    return tempoRecebido;    }
    public InetAddress getIpOwnerParaDownload() {return this.ipOwnerParaDownload;}
    public int getPortaTCPparaDownload(){ return this.portaTCPparaDownload;}

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.pcName);
        hash = 71 * hash + Objects.hashCode(this.nomeFicheiro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FicheiroDistante other = (FicheiroDistante) obj;
        if (!Objects.equals(this.pcName, other.pcName)) {
            return false;
        }
        if (!Objects.equals(this.nomeFicheiro, other.nomeFicheiro)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(FicheiroDistante o) {
        int nPc = this.pcName.compareTo(o.getNomeFicheiro());
        if(nPc!= 0) return nPc;
        int nFicheiro = this.nomeFicheiro.compareTo(o.getNomeFicheiro());
        return nFicheiro;
    }
    
    
}

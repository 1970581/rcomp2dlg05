/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import static java.lang.Math.random;

/**
 *
 * @author Hugo
 */
public abstract class Utilitario {
    
    /**
     * Confirma um nome de PC, se incorrecto devolve um aleatorio.
     * @param nomeIndicado
     * @return um nome de PC correcto
     */
    public static String confirmarNomeDestePc(String nomeIndicado){
        if ( nomeIndicado == null || nomeIndicado.isEmpty()) return gerarNomePc();
        byte byteNome[] = nomeIndicado.getBytes();
        if(byteNome.length > 8) return gerarNomePc();
        else return nomeIndicado;
    }
    
    /**
     * Gera um nome aleatorio para um PC.
     * @return um nome de PC.
     */
    public static String gerarNomePc(){
        double n = Math.random();
        int numero = (int) (n * 10000);
        String nome = "pc";
        nome = nome.concat(String.valueOf(numero));
        if (nome.length() < 8) return nome;
        else return "default";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Lida com o envio de pedidos de ficheiros.
 * @author Hugo
 */
public class TCP_Out implements Runnable{
    
    private PedidoDownload pedidoDownload;
    private Shared shared;
    private String diretorioSave;
    private InetAddress ipDestino;
    private Socket sock;
    private int serverPort;
    
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private TCPpacket tcp;
    
    public TCP_Out(PedidoDownload pedidoDownload, Shared shared, String diretorioSave){
        this.pedidoDownload = pedidoDownload;
        this.shared = shared;
        this.diretorioSave = diretorioSave;
        this.ipDestino = pedidoDownload.getIp();
        this.serverPort = pedidoDownload.getPortaRemota();
    }

    @Override
    public void run() {
        
        this.ipDestino = pedidoDownload.getIp();
        System.out.println("TCP_Out: Starting connection to: " + ipDestino.getHostAddress() + " : " + this.serverPort);
        
        try {
            // Criar ligacao
            sock = new Socket(ipDestino, this.serverPort);
            
            // Abrir os pipes.
            sOut    = new DataOutputStream( sock.getOutputStream() );
            sIn     = new DataInputStream(  sock.getInputStream()  );
            
            //Criar o header de pedido.
            String filename = pedidoDownload.getNomeFicheiro();
            byte byteFilename[] = filename.getBytes();
            
            tcp = new TCPpacket(TCPpacket.MessageType.MESSAGE_ASK_TO_DOWNLOAD, byteFilename.length);
            
            
            //Enviar o header
            sOut.write(tcp.readHeaderPayload());
            
            //Enviar o nome do ficheiro.
            sOut.write(byteFilename);
            
            //Ler resposta.
            boolean resposta1Ok = readTCP();
            
            //Se sair, sair.
            if(tcp.isMessageType(TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT)){
                System.out.println("TCP_Out: Received exit mesage.");
                sock.close();
                return;
            }
            
            //Se ficheiro, ler ficheiro
            boolean fileWriteSucess = false;
            if(tcp.isMessageType(TCPpacket.MessageType.MESSAGE_FILE)){
                fileWriteSucess = this.saveFile(filename, tcp.readDataSize());
            }
            
            if(fileWriteSucess) System.out.println("TCP_Out: File writen with sucess. -> " + filename);
            else System.out.println("TCP_Out: File write FAILURE. -> " + filename);
            
            
            sIn.close();
            sOut.close();
            
            System.out.println("TCP_Out: End of process.");
            sock.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(TCP_Out.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("TCP_Out: exception: "+ ex);
        }
        
        
        
    }
    
    
        /**
     * Funcao que le 32 bytes e os coloca na variavel this.tcp 
     * @return falso se apanhou um erro, true se tudo ok.
     * @throws IOException 
     */
    private boolean readTCP() throws IOException{
        
        int headerSize = TCPpacket.HEADER_SIZE;
        int nBytesLidos = 0;
        
        byte packetHeader[] = new byte[headerSize];
        nBytesLidos = sIn.read(packetHeader);
        if(nBytesLidos != headerSize) {
            return false;
        }
        tcp = new TCPpacket(packetHeader);
        return tcp.isMyProtocol();
    }
    
    
    private boolean saveFile(String filename, int byteSize) throws  IOException{
        String completeFilePath = this.diretorioSave + filename;
        FileOutputStream fos;
        
        byte file[] = new byte[byteSize];
        int bytesRead = 0;
        //bytesRead = sIn.read(file);
        
        int pass = 0;
        int missing = byteSize;
        
        while(missing >0){
            pass = sIn.read(file, bytesRead, missing);
            if(pass <= 0) break;
            missing -= pass;
            bytesRead += pass;
        }
        
        
        
        if(bytesRead != byteSize){ 
            System.out.println("TCP_Out: Filesize error. Read: "+bytesRead + " Espected: " +byteSize);
            return false;
        }
        
        
        try {
            fos = new FileOutputStream( completeFilePath);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TCP_Out.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("TCP_Out: Can't create file: "+ ex);
            return false;
        }
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        
        
        
        
        bos.write(file);    // writes file
        bos.close();        // closes stream
        fos.close();        // closes file
        
        return true;
    }
    
    
    private void enviarMensagemSair() throws IOException{
        TCPpacket tcpSair = new TCPpacket(TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT, 0);
        sOut.write(tcpSair.readHeaderPayload());
    }
    
}

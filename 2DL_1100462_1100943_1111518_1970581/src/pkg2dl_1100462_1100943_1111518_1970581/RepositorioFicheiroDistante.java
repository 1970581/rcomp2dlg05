/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe repositorio da lista dos diferentes PC e seus ficheiros.
 * @author Hugo
 */
public class RepositorioFicheiroDistante {
    private List<FicheiroDistante> listaRepositorio;
    private Map<String, NodePc> listaNodePc;
    private final Semaphore semafro;    // = new Semaphore(MAX_AVAILABLE, true);
    private String nomeDestePc;
    private boolean debug;
    
    private static final long PRAZO_VALIDADE = 45 * 1000;
    private static final int  MAX_AVAILABLE  = 1;
    
    public RepositorioFicheiroDistante(String nomeDestePc , boolean debug){
        this.listaRepositorio = new ArrayList();
        this.listaNodePc = new LinkedHashMap();
        this.semafro = new Semaphore(MAX_AVAILABLE, true);
        this.nomeDestePc = nomeDestePc;
        this.debug = debug;
    };
    
    
    
    /**
     * Adicionar uma entrada de um ficheiro distante.
     * @param ficheiroDistante
     * @return
     * @throws InterruptedException 
     */
    public synchronized boolean adicionarFicheiroDistante(FicheiroDistante ficheiroDistante) throws InterruptedException{
        
        //SEMAFERO UP
        this.semafro.acquire();
        
        verificarSePassouTempo(); // Actualiza a lista, apaganto os fora de prazo de validade.
        
        if(!debug && ficheiroDistante.getPcName().equalsIgnoreCase(this.nomeDestePc)){
            this.semafro.release();
            return false;
        }
        
        //Mando remover o ficheiro distante, cujo equals compara o nomePC e nome do ficheiro, (se nao existir nao da erro) e depois adiciono.
        this.listaRepositorio.remove(ficheiroDistante);
        this.listaRepositorio.add(ficheiroDistante);
        
        //SEMAFERO DOWN
        this.semafro.release();
        
        return false;
    }
    
    
    /**
     * Criar uma lista de DTO imutaveis para depois imprimir na consola.
     * @return lista de DTO (Data Transfer Object) com o nome dos nodes/PC e nome do ficheiro.
     */
    public synchronized List<FicheiroDistanteDTO> listarFicheiros() throws InterruptedException{
        List <FicheiroDistanteDTO> lista = new ArrayList();
        
        //SEMAFERO UP
        this.semafro.acquire();
        
        verificarSePassouTempo(); // Actualiza a lista, apaganto os fora de prazo de validade.
        
        for(FicheiroDistante f : this.listaRepositorio){
            lista.add(f.criarDTO());
        }
        
        //SEMAFERO DOWN
        this.semafro.release();
        
        return lista;
    }
    
    /**
     * Verifica se ja passaram 45 segundos a cada entrade, e apaga as com mais de 45 segundos. e ORDENAMOS
     */
    private synchronized void verificarSePassouTempo(){
        
        long tempoActual = System.currentTimeMillis(); //Tempo actual
        
        //Vamos iterar e apagar da lista todas as entradas fora do prazo de validade.
        Iterator<FicheiroDistante> iter = this.listaRepositorio.listIterator();
        while (iter.hasNext()) {
            FicheiroDistante fd = iter.next();
            if( (tempoActual - fd.getTempoRecebido()) > PRAZO_VALIDADE ){
                iter.remove();
            }
        }
        
        //Ordenamos a lista 1º nomePC 2º nome ficheiro.   
        listaRepositorio.sort(null); 
    }
    
    
    /**
     * Retorna a informacao necessaria para efecturar um pedido de download.
     * @param nomePC
     * @param nomeFicheiro
     * @return 
     */
    public synchronized PedidoDownload obterDadosPedidoDownload(String nomePC, String nomeFicheiro){
        
        verificarSePassouTempo(); // Actualiza a lista, apaganto os fora de prazo de validade.
        PedidoDownload pedido = null;
        for(FicheiroDistante fd: this.listaRepositorio){
            if(fd.getPcName().equalsIgnoreCase(nomePC) && fd.getNomeFicheiro().equals(nomeFicheiro) ){
                try {
                    pedido = new PedidoDownload(
                            InetAddress.getByAddress(fd.getIpOwnerParaDownload().getAddress())
                            , fd.getPortaTCPparaDownload() , nomeFicheiro);
                } catch (UnknownHostException ex) {
                    System.out.println("RepositorioFicheiroDistante: erro a construir um ip " + ex);
                }
            }
        }
        return pedido;
    }
    
    /**
     * Classe privada para guardar a lista dos nodes/PC que mandaram informacao.
     */
    private class NodePc{
        
        private String nomePC;
        private long tempoUltimoUpdateNesteSistema = 0;
        private long tempoIndicadoNoPacote = 0; 

        public NodePc(String nomePC, long tempoIndicadoNoPacote) {
            this.nomePC = nomePC;
            this.tempoUltimoUpdateNesteSistema = System.currentTimeMillis();
            this.tempoIndicadoNoPacote = tempoIndicadoNoPacote;
        }
        
        public String getNomePC(){return this.nomePC;}
        public long getTempoUltimoUpdate(){return this.tempoUltimoUpdateNesteSistema;}
        public void setTempoUltimoUpdate(long tempo) { this.tempoUltimoUpdateNesteSistema = tempo;}
        public long getTempoIndicadoNoPacote(){return this.tempoIndicadoNoPacote;}
        public void setTempoIndicadoNoPacote(long tempo) { this.tempoIndicadoNoPacote = tempo;}
    }
    
    
}

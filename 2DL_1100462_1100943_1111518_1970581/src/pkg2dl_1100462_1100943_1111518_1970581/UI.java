/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class UI {
    
    private Controller controller;
    private static final String SAIR = "S";
    private static final String LISTAR = "L";
    private static final String DOWNLOAD = "D";
    
    
    
    public UI(String diretorioLeitura, String diretorioEscrita, String nomeDestePc, boolean debug){
        this.controller = new Controller(diretorioLeitura, diretorioEscrita, nomeDestePc, debug);
    }
    
    public void run(){
        System.out.println("Inicializar o programa.");
        
        System.out.println("Diretorio leitura: " + controller.getDiretorioLeitura());
        if(controller.existeDiretorioLeitura()) System.out.println("OK.");
        else System.out.println("Diretorio inexistente.");
        
        System.out.println("Diretorio escrita: " + controller.getDiretorioEscrita());
        if(controller.existeDiretorioEscrita()) System.out.println("OK.");
        else System.out.println("Diretorio inexistente.");
        
        System.out.println("Nome de deste PC utilizado: " + controller.getNomeDestePc() + "\n");
        
        System.out.println("Ficheiros no diretorio de partilha:    ["+controller.getDiretorioLeitura()+"]");
        List <String> listaLeitura = controller.listarConteudoDeDiretorio(controller.getDiretorioLeitura());
        for(String ficheiro : listaLeitura){
            System.out.println(ficheiro);
        }
        
        System.out.println("\nFicheiros no diretorio de recebidos:    ["+controller.getDiretorioEscrita()+"]");
        List <String> listaEscrita = controller.listarConteudoDeDiretorio(controller.getDiretorioEscrita());
        for(String ficheiro : listaEscrita){
            System.out.println(ficheiro);
        }
        
        try {controller.inicializarReceptorTCP();}
        catch (Exception e){
            System.out.println("UI: CRITICAL ERROR. initializing EXITING: \n"+e);
            return;
        }
        
        
        controller.inicializarReceptorUDP();
        System.out.println("ReceptorUDP inicializado.");
        
        
        controller.inicializarEmisorUDP();
        System.out.println("EmisorUDP inicializado.");
        
        
        //CODIGO PARA LER OS FICHEIROS DISPONIVEIS.
        
        imprimirLista();
        
        menuPrincipal();
        
        controller.exit();
    }
    
    
    private void imprimirLista(){
        System.out.println("\nLista de DOWNLOADS possiveis:");
        System.out.println("[nomePC] \"nomeDoFicheiro\"");
        List<FicheiroDistanteDTO> lista = controller.listarFicheirosPossiveisFazerDownload();
        if(lista == null || lista.isEmpty()) {
            System.out.println("Nao existem ficheiros que possa fazer download.");
            return;
        }
        for(FicheiroDistanteDTO ficheiro : lista){
            System.out.println("["+ficheiro.getPcName()+"] \""+ficheiro.getNomeFicheiro()+"\"");
        }
    }
    
    
    private void menuPrincipal(){
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        String linha = "";
        while (!linha.equalsIgnoreCase(SAIR)) {
            System.out.println("\nMenu Principal:");
            System.out.println("\""+SAIR+"\" para sair.");
            System.out.println("\""+LISTAR+"\" - para listar ficheiros passiveis de download ");
            System.out.println("\""+DOWNLOAD+"\" - para fazer um download ");
            System.out.println("Opcao:");
            
            linha = reader.nextLine();
            
            if(linha.equalsIgnoreCase(LISTAR)) {imprimirLista();continue;}
            if(linha.equalsIgnoreCase(DOWNLOAD)) {downloadFicheiro();  continue;}
            if(linha.equalsIgnoreCase(SAIR)) continue;  
            System.out.println(" ***** INVALID OPTION   *******");
            
        }
        System.out.println("Saindo... pf aguarde.");
        return;
    }
    private void downloadFicheiro(){
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Nome do PC ?");
        String pc = reader.nextLine();
        System.out.println("Nome do ficheiro ?");
        String ficheiro = reader.nextLine();
        boolean ok = controller.fazerDownload(pc, ficheiro);
        if (!ok) System.out.println("Ficheiro invalido, desactualizado ou inexistente nos pacotes recebidos.");
        if (ok) System.out.println("A inicializar processo de pedido de download.");
        
        
    }
    
}

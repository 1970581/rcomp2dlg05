/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugo
 */
public class ReceptorTCP implements Runnable{
    
    private RepositorioFicheiroDistante repositorio;
    private Shared shared;
    private String diretorioShared;
    private String nomeDestePc;
    private int portReceptorTCP = 0;
    private ServerSocket serverSocketReceptorTCP;
    private static final int TIME_OUT = 30000;// 1000 * 60 * 5;  //1 min
    
    
    public ReceptorTCP( ServerSocket serverSocketReceptorTCP , Shared shared, String diretorioShared,  String nomeDestePc ){
        this.serverSocketReceptorTCP = serverSocketReceptorTCP;
        this.shared = shared;
        this.diretorioShared = diretorioShared;
        this.nomeDestePc = nomeDestePc;
    }

    @Override
    public void run() {
        
        
        
        // Colocar um timout ao port.
        try { serverSocketReceptorTCP.setSoTimeout(ReceptorTCP.TIME_OUT); } 
        catch (SocketException ex) {
            this.terminar();
            System.out.println("Erro em new ServerSocket: " + ex);
            try { serverSocketReceptorTCP.close();}
            catch (IOException ex2){ System.out.println("Erro em new ServerSocket + close: " + ex2);}
        }
        
        this.portReceptorTCP = this.serverSocketReceptorTCP.getLocalPort();
        
        while(true){
            if(shared.terminarAplicacao(false) == true) break; // Ser for para sair SAI.
            
            Socket conectionSocket;
            try {conectionSocket = serverSocketReceptorTCP.accept();}
            catch (SocketTimeoutException ex){
                System.out.println("ReceptorTCP: ServerSocket.accept() timeout (aviso de tempo excedido, normal) " + ex);
                continue;
            }
            catch (IOException exIO){
                this.terminar();
                System.out.println("Erro em serverSocket.accept() " + exIO);
                return;
            }
            
            System.out.println("ReceptorTCP: Ligação aceitada.");
            Thread T_TCP_In = new Thread(new TCP_In(conectionSocket, shared, this.diretorioShared ));
            T_TCP_In.start();
            
        }
        
        System.out.println("ReceptorTCP: Terminando.");
    }
    
    
    /**
     * Usado para indicar a aplicacao para sair.
     */
    private void terminar(){
        this.shared.terminarAplicacao(true);
        System.out.println("ReceptorTCP: Indica sair.");
    }
    
    
}



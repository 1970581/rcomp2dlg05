/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

/**
 *
 * @author Hugo
 */
public class FicheiroDistanteDTO {
    
    private String pcName;
    private String nomeFicheiro;
    private long  tempoRecebido;
    
    
    public FicheiroDistanteDTO(String pcName, String nomeFicheiro ){
        this.pcName = pcName;
        this.nomeFicheiro = nomeFicheiro;        
    }
    public String getPcName() {        return pcName;    }
    public String getNomeFicheiro() {  return nomeFicheiro;    }    
}

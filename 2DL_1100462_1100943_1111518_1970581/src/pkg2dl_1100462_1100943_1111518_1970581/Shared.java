/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

/**
 *
 * @author Hugo
 */
public class Shared {
    
    /** boleano que guarda se devemos terminar a aplicacao*/
    private boolean exit;
    
    public Shared(){
        this.exit = false;
    }
    
    /** Questiona se devemos terminar aplicacao.
     * @param forceExit deve ser falso, excepto se queremos indicar que e para terminar.
     * @return true - terminar aplicacao, false - continuar.
    */
    public synchronized boolean terminarAplicacao(boolean forceExit){
        if (forceExit == true) this.exit = true;
        return this.exit;
    }
    
   
}

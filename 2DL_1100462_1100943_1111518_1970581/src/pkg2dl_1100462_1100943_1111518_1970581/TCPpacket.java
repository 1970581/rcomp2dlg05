/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Hugo
 */
public class TCPpacket {
    
    
    private byte[] headerPayload;
    
    public static final int HEADER_SIZE                 = 32;
    
    public static final int TCP_START__PROTOCOL_NAME    = 0;
    public static final int TCP_LENGTH_PROTOCOL_NAME    = 8;
    
    public static final int TCP_START__VERSION          = 9;
    public static final int TCP_LENGTH_VERSION          = 1;
    
    public static final int TCP_START__MESSAGE_TYPE     = 10;
    public static final int TCP_LENGTH_MESSAGE_TYPE     = 8;
    
    public static final int TCP_START__DATA_LENGTH      = 19;
    public static final int TCP_LENGTH__DATA_LENGTH     = 4;
    
    
    //public static final int MESSAGE_ASK_TO_EXIT         = 0;
    //public static final int MESSAGE_ASK_TO_DOWNLOAD     = 1;
    
    public enum MessageType {MESSAGE_ASK_TO_EXIT, MESSAGE_ASK_TO_DOWNLOAD , MESSAGE_FILE};
    
    
    /** Tipos de mensagens aceitaveis. */
    public static final byte[] BYTES8_ASK_TO_EXIT        = { 69, 88, 73, 84, 0, 0, 0, 0};  //EXIT
    public static final byte[] BYTES8_ASK_TO_DOWNLOAD    = { 68, 79, 87, 78, 76, 79, 65, 68};  //DOWNLOAD
    public static final byte[] BYTES8_FILE               = { 70, 73, 76, 69, 0, 0, 0, 0};  //FILE
    
    public static final byte[] BYTES8_PROTOCOL_NAME      = { 84, 67, 80, 82, 67, 79, 77, 80};    //TCPRCOMP
    
    public static final byte   BYTES1_VERSION_NUMBER    = 1;
    
    
    /*
    Dec Hex	Binary          Character	Description
    65 	41 	01000001 	A
    66 	42 	01000010 	B 	 
    67 	43 	01000011 	C 	 
    68 	44 	01000100 	D 	 
    69 	45 	01000101 	E 	 
    70 	46 	01000110 	F 	 
    71 	47 	01000111 	G 	 
    72 	48 	01001000 	H 	 
    73 	49 	01001001 	I 	 
    74 	4A 	01001010 	J 	 
    75 	4B 	01001011 	K 	 
    76 	4C 	01001100 	L 	 
    77 	4D 	01001101 	M 	 
    78 	4E 	01001110 	N 	 
    79 	4F 	01001111 	O 	 
    80 	50 	01010000 	P 	 
    81 	51 	01010001 	Q 	 
    82 	52 	01010010 	R 	 
    83 	53 	01010011 	S 	 
    84 	54 	01010100 	T 	 
    85 	55 	01010101 	U 	 
    86 	56 	01010110 	V 	 
    87 	57 	01010111 	W 	 
    88 	58 	01011000 	X 	 
    89 	59 	01011001 	Y 	 
    90 	5A 	01011010 	Z
    */
    
    // BITS da HEADER
    // 0-7      nome do protocolo
    // 8        0 - vazio
    // 9        versao
    // 10-17    tipo de mensagem
    // 18       0 - vazio
    // 19-22    data length , int.
    // 23-31    0 - vazio.
    
    public TCPpacket( MessageType messageType, int dataLength){
        //Construir o payload.
        this.headerPayload = new byte[TCPpacket.HEADER_SIZE];
        
        //Nome protocolo:
        System.arraycopy(BYTES8_PROTOCOL_NAME  , 0, this.headerPayload, TCP_START__PROTOCOL_NAME, TCP_LENGTH_PROTOCOL_NAME);
        
        //Versao
        this.headerPayload[TCP_START__VERSION] = BYTES1_VERSION_NUMBER;
        
        //Typo de mensagem
        switch(messageType){
            case MESSAGE_ASK_TO_DOWNLOAD:   System.arraycopy(BYTES8_ASK_TO_DOWNLOAD  , 0, this.headerPayload, TCP_START__MESSAGE_TYPE, TCP_LENGTH_MESSAGE_TYPE);
                break;
            case MESSAGE_ASK_TO_EXIT:       System.arraycopy(BYTES8_ASK_TO_EXIT      , 0, this.headerPayload, TCP_START__MESSAGE_TYPE, TCP_LENGTH_MESSAGE_TYPE);
                break;
            case MESSAGE_FILE:              System.arraycopy(BYTES8_FILE             , 0, this.headerPayload, TCP_START__MESSAGE_TYPE, TCP_LENGTH_MESSAGE_TYPE);
                break;
            default:                        System.arraycopy(BYTES8_ASK_TO_EXIT      , 0, this.headerPayload, TCP_START__MESSAGE_TYPE, TCP_LENGTH_MESSAGE_TYPE);
                break;
        }
        
        //Comprimento da mensagem:
        ByteBuffer bufferTamanhoData = ByteBuffer.allocate(TCP_LENGTH__DATA_LENGTH);
        bufferTamanhoData.putInt(dataLength);
        byte dataSize[] = bufferTamanhoData.array();
        System.arraycopy( dataSize , 0, this.headerPayload, TCP_START__DATA_LENGTH, TCP_LENGTH__DATA_LENGTH);
        
    }
    
    /** Construtor vazio */
    public TCPpacket(){
        this(TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT, 0); //Chama o construtor com parametrods.
    }
    
    /** Constroi um TCP packet com base numa header em 32 bytes */
    public TCPpacket(byte headerPayload[]){
        if (headerPayload.length != HEADER_SIZE) throw new IllegalArgumentException("Payload size was not 32 bytes.");
        this.headerPayload = headerPayload.clone();
    }
    
    
   /** Retorna o payload byte[] do header to packet    */
    public byte[] readHeaderPayload(){
        return this.headerPayload.clone();
    }
    
    /** Retorna o tamanho da data indicada no header do pacote */
    public int readDataSize(){
        byte writenDataSizeBytes[] = new byte[TCP_LENGTH__DATA_LENGTH];
        
        System.arraycopy(this.headerPayload, TCP_START__DATA_LENGTH, writenDataSizeBytes, 0, TCP_LENGTH__DATA_LENGTH);
        
        ByteBuffer bufferDataSize = ByteBuffer.wrap(writenDataSizeBytes);
        return bufferDataSize.getInt();
        
    }
    
    
    
    /** Verifica se e de um tipo de mensagem */
    public boolean isMessageType(MessageType messageType){
        
        byte writenMessageBytes[] = new byte[TCP_LENGTH_MESSAGE_TYPE];
        
        System.arraycopy(this.headerPayload, TCP_START__MESSAGE_TYPE, writenMessageBytes, 0, TCP_LENGTH_MESSAGE_TYPE);
        
        switch(messageType){
            case MESSAGE_ASK_TO_DOWNLOAD:   return Arrays.equals(writenMessageBytes, BYTES8_ASK_TO_DOWNLOAD);
            case MESSAGE_ASK_TO_EXIT:       return Arrays.equals(writenMessageBytes, BYTES8_ASK_TO_EXIT);
            case MESSAGE_FILE:              return Arrays.equals(writenMessageBytes, BYTES8_FILE);
            default:                        //DO nothing.                       
        }
        return false;
    }
    
    //* Verifica se e o nosso protocolo, confirmando o nome da mensagem */
    public boolean isMyProtocol(){
        byte writenProtocolNameBytes[] = new byte[TCP_LENGTH_PROTOCOL_NAME];
        
        System.arraycopy(this.headerPayload, TCP_START__PROTOCOL_NAME, writenProtocolNameBytes, 0, TCP_LENGTH_PROTOCOL_NAME);
        
        return Arrays.equals(writenProtocolNameBytes, BYTES8_PROTOCOL_NAME );
    }
    
    
}

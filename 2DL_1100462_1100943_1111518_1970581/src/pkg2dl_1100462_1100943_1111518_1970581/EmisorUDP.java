/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugo
 */
public class EmisorUDP implements Runnable{
    private Shared shared;   
    private String diretorioLeitura;
    private String nomeDestePc;
    private int portEscutaTCP;
    
    private final InetAddress IPdestino;
    
    private static final String DEFAULT_IP = "255.255.255.255";
    private static final int TIME_WAIT_BETWEN_PACKETS = 30000; // 30 segundos
    public static final int PORT_ENVIO_BROADCAST = 32119;
    public static final int PORT_RECEPCAO_BROADCAST = 32120;
    
    
    public EmisorUDP(String diretorioL, String nomeDestePc, InetAddress destino, int portEscutaTCP, Shared shared){
        this.shared = shared;
        this.diretorioLeitura = diretorioL;
        this.nomeDestePc = nomeDestePc;
        this.portEscutaTCP = portEscutaTCP;
        
        InetAddress IPdefault;// usado abaixo.
        
        //Converter o default em objecto endresso IP
        try{
            IPdefault = InetAddress.getByName(DEFAULT_IP);
        }
        catch(UnknownHostException e){  //NUNCA OCORRE
            System.out.println("EMISOR UDP ERROR " + e.getMessage());
            System.out.println("NUNCA DEVIA OCORRER UM ERRO AQUI");
            throw new IllegalArgumentException();
        }
        
        if (destino != null) this.IPdestino = destino;
        else IPdestino = IPdefault;
        
    }

    @Override
    public void run() {
        System.out.println("EmisorUDP online.");
        
        while(true){
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.
            
            //Envia a lista.
            int nEnviados = this.enviarLista();
            System.out.println("Broadcast: "+ nEnviados + " pacotes.");
            
            int tempoEspera = TIME_WAIT_BETWEN_PACKETS/6;  // 5 segundos.
            
            // x6 para 30 segundos. E nao termos de esperar 30 segundos para sair. Assim max = 5 seg.
            waitTime(tempoEspera);
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.
            waitTime(tempoEspera);
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.waitTime(tempoEspera);
            waitTime(tempoEspera);
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.waitTime(tempoEspera);
            waitTime(tempoEspera);
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.waitTime(tempoEspera);
            waitTime(tempoEspera);
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.waitTime(tempoEspera);
            waitTime(tempoEspera);
            if (shared.terminarAplicacao(false) == true) break; //symcronized read.waitTime(tempoEspera);
        }
        
        System.out.println("EmisorUDP terminating...");
    }
    
    /**
     * Envia a lista de ficheiros atravez de UDP
     * @return 
     */
    private int enviarLista(){
        //Obter a lista.
        List<String> listaFicheiros = ListarDiretorio.listarFicheiros(this.diretorioLeitura);
        
        if(listaFicheiros.isEmpty())return 0; //Se lista vazia voltamos.
        
        //Campos do UDPpacket
        String nomeProtocolo = UDPpacket.PROTOCOL_NAME;
        byte versao = 1;
        long tempo = System.currentTimeMillis();
        boolean insersao;
        int numeroPacotesEnviados = 0;

        UDPpacket pacote = new UDPpacket(nomeProtocolo, versao, this.nomeDestePc, this.portEscutaTCP, tempo);

        for (String nomeFicheiro : listaFicheiros) {

            insersao = pacote.inserirFicheriroNoPayload(nomeFicheiro);
            if (insersao == false) {
                broadcastPacketUDP(pacote);
                numeroPacotesEnviados++;
                pacote = new UDPpacket(nomeProtocolo, versao, this.nomeDestePc, this.portEscutaTCP, tempo);
            }
        }
        broadcastPacketUDP(pacote);
        numeroPacotesEnviados++;

        return numeroPacotesEnviados;
    }
    
    
    private boolean broadcastPacketUDP(UDPpacket udpOut){
        
        //Criar o socket
        DatagramSocket sock;
        try {       sock = new DatagramSocket(PORT_ENVIO_BROADCAST);
        } catch (SocketException ex) {
            //Tratar o erro.
            Logger.getLogger(EmisorUDP.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR EmisorUDP DatagramSocket Creation Error: " + ex.getMessage());
            this.shared.terminarAplicacao(true);
            return false;
        }
        
        //Criar o datagrama
        byte data[] = udpOut.obterPayload();
        DatagramPacket udpPacket = new DatagramPacket(data, data.length, this.IPdestino, PORT_RECEPCAO_BROADCAST);
        
        //Enviar o datagrama no socket
        try {            sock.send(udpPacket);
        } catch (IOException ex) {
            Logger.getLogger(EmisorUDP.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR EmisorUDP DatagramSocket Send Error: " + ex.getMessage());
            this.shared.terminarAplicacao(true);
            sock.close();
            return false;
        }
        System.out.println("EmisorUDP pacote enviado com sucesso.");
        sock.close();
        return true;
    }
    
    
    /**
     * Espera X segundos.
     * @param tempoDeEspera tempo que deve aguardar.
     */
    private void waitTime(int tempoDeEspera){
        try { 
                //System.out.println("Sleeping 10");
                sleep(tempoDeEspera);
                 } 
            catch (InterruptedException ex) {
                Logger.getLogger(EmisorUDP.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
}

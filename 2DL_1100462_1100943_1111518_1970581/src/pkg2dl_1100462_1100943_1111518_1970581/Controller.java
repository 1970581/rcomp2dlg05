/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hugo
 */
public class Controller {
    RepositorioFicheiroDistante repositorio;
    private String diretorioLeitura;
    private String diretorioEscrita;
    private Shared shared;
    private String nomeDestePc;
    private int portDeEscutaTCP = 0;
    private boolean debug;
    
    private ServerSocket serverSocketReceptorTCP;
    
    
    public Controller(String diretorioLeitura, String diretorioEscrita, String nomeDestePc, boolean debug){
        this.debug = debug;
        
        //Confirmar diretorio de leitura, se inexistente usamos default.
        boolean dirLeituraOk = ListarDiretorio.diretorioExiste(diretorioLeitura);
        if (dirLeituraOk)        this.diretorioLeitura = diretorioLeitura;
        else this.diretorioLeitura = ListarDiretorio.DEFAULT_DIRECTORY;
        
        //Confirmar o de escrita
        boolean dirEscritaOk = ListarDiretorio.diretorioExiste(diretorioEscrita);
        if (dirEscritaOk)        this.diretorioEscrita = diretorioEscrita;
        else this.diretorioEscrita = ListarDiretorio.DEFAULT_DIRECTORY;
        
        //Confirmar e corrigir o nome deste pc para 8 chars.
        this.nomeDestePc = Utilitario.confirmarNomeDestePc(nomeDestePc);
        
        //Criar o repositorio.
        repositorio = new RepositorioFicheiroDistante(this.nomeDestePc, this.debug);
        
        //Coloca a variavel indicadora de saida a false;
        this.shared = new Shared();
        
        
        try { serverSocketReceptorTCP = new ServerSocket(portDeEscutaTCP); } // fazemos set a 0 para ele escolher automaticamente.
        catch(IOException ex){
                System.out.println("Erro em new ServerSocket: " + ex);
                throw new IllegalStateException("ServerSocket creation error." + ex);
        }
        this.portDeEscutaTCP = this. serverSocketReceptorTCP.getLocalPort();
    }
    
    /** indica ao controller para sair */
    public void exit(){this.shared.terminarAplicacao(true);}
    
    public boolean existeDiretorio(String dir){return ListarDiretorio.diretorioExiste(dir);}
    
    public List<String> listarConteudoDeDiretorio(String dir){return ListarDiretorio.listarFicheiros(dir);}
    
    public String getDiretorioLeitura() {return this.diretorioLeitura;}
    public String getDiretorioEscrita() {return this.diretorioEscrita;}
    public String getNomeDestePc() {return this.nomeDestePc;}
    
    public boolean existeDiretorioLeitura(){return existeDiretorio(this.diretorioLeitura);}
    public boolean existeDiretorioEscrita(){return existeDiretorio(this.diretorioEscrita);}
    
    public void inicializarEmisorUDP(){
          //(new Thread(new EmisorUDP(this.diretorioLeitura, null, this.shared))).start();
          Thread T_emisorUDP = new Thread(new EmisorUDP(diretorioLeitura, nomeDestePc, null, this.portDeEscutaTCP, shared));
          T_emisorUDP.start();
    }
    
    public void inicializarReceptorUDP(){
          //(new Thread(new EmisorUDP(this.diretorioLeitura, null, this.shared))).start();
          Thread T_receptorUDP = new Thread(new ReceptorUDP( this.repositorio, this.shared));
          T_receptorUDP.start();
    }
    
    
    public void inicializarReceptorTCP(){
        Thread T_receptorUDP = new Thread(new ReceptorTCP(serverSocketReceptorTCP, shared, diretorioLeitura, nomeDestePc));
        T_receptorUDP.start();
    }
    
    
    
    
    /** Devolve a lista de DTO para o UI imprimir */
    public List<FicheiroDistanteDTO> listarFicheirosPossiveisFazerDownload(){
        List<FicheiroDistanteDTO> lista = null;
        try {lista = this.repositorio.listarFicheiros();}
        catch(InterruptedException e){System.out.println("listarFicheirosPossiveisFazerDownload() exception. : " + e.getMessage());}
        return lista;
    }
    
    /** Fazer um download */
    public boolean fazerDownload(String nomePC, String nomeFicheiro) {
        
        PedidoDownload pedido = this.repositorio.obterDadosPedidoDownload(nomePC, nomeFicheiro);
        if (pedido == null) return false;
        Thread T_TCP_Out = new Thread(new TCP_Out(pedido , shared, this.diretorioEscrita));
        T_TCP_Out.start();
        return true;
    }
    
    /** verifica se existe alguma indicacao para terminar. */
    public boolean isToTerminate(){
        return this.shared.terminarAplicacao(false);
    }
    
}

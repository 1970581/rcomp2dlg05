/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.util.List;

/**
 *
 * @author Hugo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        if(args.length < 2)System.out.println("PROGRAM ARGUMENTS: [shared directory] [download directory] [pcName] \nExample \" d:/testc/shared/ d:/testc/download/ pcruca\"");
        
        
        
        // test
        String diretorioLeitura = "d:/testc/shared/";
        String diretorioEscrita = "d:/testc/download/";
        //String nomeDestePc = "pchugo";
        String nomeDestePc = "pchugodddddddddddddddd";
        boolean debug = false;
        
        if(args.length >=2){
            diretorioLeitura = args[0];
            diretorioEscrita = args[1];
            if(args.length >= 3) nomeDestePc = args[2];
            if(args.length >= 4 && args[3].equalsIgnoreCase("debug")) debug = true;
        }
        if(debug)System.out.println("**********************************  DEBUG MODE *");
        
        UI ui = new UI(diretorioLeitura, diretorioEscrita, nomeDestePc, debug);
        ui.run();
        
        
        
    }
    
}

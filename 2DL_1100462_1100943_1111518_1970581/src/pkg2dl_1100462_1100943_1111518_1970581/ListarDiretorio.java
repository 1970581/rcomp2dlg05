/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.io.File;
import java.nio.file.Files;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hugo
 */
public abstract class ListarDiretorio {
    
    private static String directory = "d:/testc";
    public static final String DEFAULT_DIRECTORY = "./";
    
    public ListarDiretorio(){}
    
    /**
     * Lista todos os ficheiros existentes num diretorio.
     * @param dir path para o diretorio.
     * @return lista de strings com o nome dos ficheiros.
     */
    public static List<String> listarFicheiros(String dir){
        List lista = new ArrayList();
        directory = DEFAULT_DIRECTORY;
        
        boolean existe = diretorioExiste(dir);
        //System.out.println(existe);
        if(!existe) return null;
        
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && !listOfFiles[i].isHidden()) {
                //System.out.println("File " + listOfFiles[i].getName());
                
                lista.add(listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                //System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
        
        return lista;
    }
    
    public static boolean diretorioExiste(String dir){
        File test = new File(dir);
        boolean exists = test.isDirectory() && test.canRead();
        return exists;
    }
    
    
    public static boolean ficheiroExiste(String dir, String filename){
        return listarFicheiros(dir).contains(filename);
    }
    
    
}

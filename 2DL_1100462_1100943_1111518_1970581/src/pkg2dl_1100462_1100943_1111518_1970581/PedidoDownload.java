/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.net.InetAddress;

/**
 *
 * @author Hugo
 */
public class PedidoDownload {
    private int portaRemota;
    private InetAddress ip;
    private String nomeFicheiro;

    public PedidoDownload( InetAddress ip, int portaRemota, String nomeFicheiro) {
        this.portaRemota = portaRemota;
        this.ip = ip;
        this.nomeFicheiro = nomeFicheiro;
    }
    
    /**
     * @return the portaRemota
     */
    public int getPortaRemota() {
        return portaRemota;
    }   

    /**
     * @return the ip
     */
    public InetAddress  getIp() {
        return ip;
    }

    /**
     * @return the nomeFicheiro
     */
    public String getNomeFicheiro() {
        return nomeFicheiro;
    }
    
    
}

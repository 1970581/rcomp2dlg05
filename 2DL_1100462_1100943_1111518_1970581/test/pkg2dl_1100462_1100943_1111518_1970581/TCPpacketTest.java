/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class TCPpacketTest {
    
    TCPpacket tcp1;
    TCPpacket tcp2;// j
    TCPpacket tcp3;
    
    
    public TCPpacketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        tcp1 = new TCPpacket(TCPpacket.MessageType.MESSAGE_FILE, 24000);
        tcp2 = new TCPpacket();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readHeaderPayload method, of class TCPpacket.
     */
    @Test
    public void testReadHeaderPayload() {
        System.out.println("readHeaderPayload");
        TCPpacket instance = new TCPpacket(TCPpacket.MessageType.MESSAGE_FILE, tcp1.readDataSize());
        byte[] expResult = tcp1.readHeaderPayload();
        byte[] result = instance.readHeaderPayload();
        assertArrayEquals(expResult, result);
        
    }

    /**
     * Test of isMessageType method, of class TCPpacket.
     */
    @Test
    public void testIsMessageType() {
        System.out.println("isMessageType");
        TCPpacket.MessageType messageType = TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT;
        TCPpacket instance = tcp1;
        boolean expResult = false;
        boolean result = instance.isMessageType(messageType);
        assertEquals(expResult, result);
        
        assertTrue(new TCPpacket(TCPpacket.MessageType.MESSAGE_FILE, 0).isMessageType(TCPpacket.MessageType.MESSAGE_FILE));
        assertTrue(new TCPpacket(TCPpacket.MessageType.MESSAGE_ASK_TO_DOWNLOAD, 0).isMessageType(TCPpacket.MessageType.MESSAGE_ASK_TO_DOWNLOAD));
        assertTrue(new TCPpacket(TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT, 0).isMessageType(TCPpacket.MessageType.MESSAGE_ASK_TO_EXIT));
    }

    /**
     * Test of isMyrotocol method, of class TCPpacket.
     */
    @Test
    public void testIsMyProtocol() {
        System.out.println("isMyProtocol");
        TCPpacket instance = new TCPpacket();
        boolean expResult = true;
        boolean result = instance.isMyProtocol();
        assertEquals(expResult, result);
        assertTrue(tcp1.isMyProtocol());
    }

    /**
     * Test of readDataSize method, of class TCPpacket.
     */
    @Test
    public void testReadDataSize() {
        System.out.println("readDataSize");
        TCPpacket instance = tcp1;
        int expResult = 24000;
        int result = instance.readDataSize();
    }
    
}

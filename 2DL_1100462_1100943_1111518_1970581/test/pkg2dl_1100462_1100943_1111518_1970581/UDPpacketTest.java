/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dl_1100462_1100943_1111518_1970581;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class UDPpacketTest {
    
    UDPpacket udp1;
    UDPpacket udp2;
    UDPpacket udp3;
    byte versao;
    String nomePc;
    int port;
    long tempo;
    String nomePc2;
    List<String> listaFicheiros;
    
    public UDPpacketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        versao = 1;
        nomePc = "X11";
        port = 32500;
        tempo = 21;
        udp1 = new UDPpacket(UDPpacket.PROTOCOL_NAME, versao, nomePc, port, tempo);
        nomePc2 = "Z13";
        udp2 = new UDPpacket(UDPpacket.PROTOCOL_NAME, versao, nomePc2, port, tempo);
        udp3 = new UDPpacket(udp1.obterPayload());
     
        listaFicheiros = new ArrayList();
        listaFicheiros.add("ficheiro1.txt");
        listaFicheiros.add("ficheiro2.txt");
        listaFicheiros.add("ficheiro3.txt");
        listaFicheiros.add("ficheiro4.txt");
        listaFicheiros.add("ficheiro5.txt");
        listaFicheiros.add("ficheiro6.txt");
        listaFicheiros.add("ficheiro7.txt");
        listaFicheiros.add("ficheiro8.txt");
        listaFicheiros.add("ficheiro9.txt");
        listaFicheiros.add("ficheiro10.txt");
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of obterPayload method, of class UDPpacket.
     */
    @Test
    public void testObterPayload() {
        System.out.println("obterPayload");
        UDPpacket instance = udp1;
        byte[] expResult = udp3.obterPayload();
        byte[] result = instance.obterPayload();
        assertArrayEquals(expResult, result);
        
        boolean pass = false;
        try{
        assertArrayEquals(udp1.obterPayload(), udp2.obterPayload());
        
        }
        catch(AssertionError ae){pass = true;}
        assertTrue(pass);
        
        assertEquals(udp1.getNomeProtocolo(), udp3.getNomeProtocolo());
        assertEquals(udp1.getNomeProtocolo(), udp2.getNomeProtocolo());
        assertFalse(udp1.getNomePc().equals(udp2.getNomePc()));
    }

    /**
     * Test of inserirFicheriroNoPayload method, of class UDPpacket.
     */
    @Test
    public void testInserirFicheriroNoPayload() {
        System.out.println("inserirFicheriroNoPayload");
        String nomeFicheiro = "ficheiro1";
        UDPpacket instance = udp1;
        boolean expResult = true;
        boolean result = instance.inserirFicheriroNoPayload(nomeFicheiro);
        assertEquals(expResult, result);      
        String grande = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        assertTrue(udp1.inserirFicheriroNoPayload(grande));
        assertTrue(udp1.inserirFicheriroNoPayload(grande));
        assertTrue(udp1.inserirFicheriroNoPayload(grande));
        assertTrue(udp1.inserirFicheriroNoPayload(grande));
        assertFalse(udp1.inserirFicheriroNoPayload(grande));
        
        List<String> lista = udp1.obterListaFicheiro();
        assertTrue(lista.size() == 5);
        assertTrue(lista.get(4).equals(grande));
    }

    /**
     * Test of obterListaFicheiro method, of class UDPpacket.
     */
    @Test
    public void testObterListaFicheiro() {
        System.out.println("obterListaFicheiro");
        UDPpacket instance = udp1;
        
        for(String s : listaFicheiros){ udp1.inserirFicheriroNoPayload(s);}
        
        
        List<String> expResult = listaFicheiros;
        List<String> result = instance.obterListaFicheiro();
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of getNomeProtocolo method, of class UDPpacket.
     */
    @Test
    public void testGetNomeProtocolo() {
        System.out.println("getNomeProtocolo");
        UDPpacket instance = udp1;
        String expResult = UDPpacket.PROTOCOL_NAME;
        String result = instance.getNomeProtocolo();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getVersao method, of class UDPpacket.
     */
    @Test
    public void testGetVersao() {
        System.out.println("getVersao");
        UDPpacket instance = udp1;
        byte expResult = versao;
        byte result = instance.getVersao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNomePc method, of class UDPpacket.
     */
    @Test
    public void testGetNomePc() {
        System.out.println("getNomePc");
        UDPpacket instance = udp1;
        String expResult = nomePc;
        String result = instance.getNomePc();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPorta method, of class UDPpacket.
     */
    @Test
    public void testGetPorta() {
        System.out.println("getPorta");
        UDPpacket instance = udp1;
        int expResult = port;
        int result = instance.getPorta();
        assertEquals(expResult, result);        
    }

    /**
     * Test of getTempo method, of class UDPpacket.
     */
    @Test
    public void testGetTempo() {
        System.out.println("getTempo");
        UDPpacket instance = udp1;
        long expResult = tempo;
        long result = instance.getTempo();
        assertEquals(expResult, result);
    }

    
    
}
